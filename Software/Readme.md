### Nodo ###

Parte de software para cada nodo. Desarrollado sobre Arduino IDE para ESP8266 Wemos D1.

Se basa en esta [libreria](https://github.com/sandeepmistry/arduino-LoRa)

Estado actual: usando los ejemplos de la libreria para manterner una pequeña comunicacion.


### TODO ###

A subir cuando este un poco mas maduro, de momento son solo los ejemplos.

No prioritario.
