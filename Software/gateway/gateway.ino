/*
   Francisco Sanchez Lopez - 2021 Sevilla

   TFG - Universidad de Sevilla

   Programa principal Gateway
*/

#define GW_ADDR 0x00

#include <SPI.h>
#include <LoRa.h>

#define csPin     15
#define resetPin  16
#define irqPin    5
#define LoraFreq  433E6

#define eventPin  4

long lastSendTime = 0;
int interval = 2000;

long lastSendTime2 = 0;
int interval2 = 2000;

/*
   FCSA
*/

static uint16_t seq = 0;
static uint32_t base = 0;
static uint32_t lastupdate = 0;
static double lrate = 1.0f;

static int fcsa_reset;

#define NODE_LIST_LEN 5
#define NODE_DATA_LEN 20

struct node_data {
  uint8_t activo;
  double hrate;
  double lrate;
  uint8_t tam, in;
  uint32_t local[NODE_DATA_LEN];
  uint32_t neig[NODE_DATA_LEN];
};

static struct node_data nodos_tabla[NODE_LIST_LEN];

/******************
   PAQUETES
*/

enum paquete_tipo {
  FCSA_BEACON = 0x01,
  FCSA_JOIN   = 0x02,
  DATA        = 0x03
};

typedef struct {
  uint16_t seq;
  uint32_t logical;
  uint32_t hardware;
  double lrate;
} fcsa_beacon;

typedef struct {
  uint16_t len;
  uint32_t ts;
} data_frame;

/*
   PRINCIPAL
*/

void setup() {
  //Inicializar Serial
  Serial.begin(115200);
  while (!Serial);
  Serial.println("*");
  Serial.println("*");
  Serial.println("*");
  Serial.println("*");
  Serial.println("** Sincronizador temporal para IoT basado en hardware **");
  Serial.println("** Francisco Sanchez - TFG - 2020/21 - DTE **");

  //Inicializar Lora
  LoRa.setPins(csPin, resetPin, irqPin);
  Serial.print("Inicializando modulo Lora ");
  if (!LoRa.begin(LoraFreq)) {
    Serial.println("Fallo!");
    while (true);
  }
  Serial.println("OK!");

  Serial.print("Registrando callback ");
  LoRa.onReceive(procesar_paquete);
  Serial.println("OK");

  Serial.print("Activando escucha ");
  LoRa.receive();
  Serial.println("OK");

  init_fcsa();

  Serial.printf("Local addr: 0x%02hhx\r\n", GW_ADDR);

  Serial.print("Enviando Join ");
  enviar_fcsa_join();
  Serial.println("OK");

  pinMode(eventPin, INPUT_PULLUP);
  attachInterrupt(eventPin, event1_isr, HIGH);

  Serial.println("Inicializacion completa!");
}

int eventLast;
int eventInterval;

uint32_t evento1pendiente = 0;

ICACHE_RAM_ATTR void event1_isr() {
  if (millis() - eventLast > eventInterval) {
    Serial.println("Evento");

    evento1pendiente = fcsa_get_time();
    Serial.printf("< JSON {\"saddr\":\"0\",\"ts\":%i,\"content\":\"evento2\"} >\r\n", fcsa_get_time());

    eventLast = millis();
    eventInterval = 400;
  }

}

void loop() {
  if (millis() - lastSendTime > interval) {
    Serial.println("< Enviando FCSA beacon >");
    seq++;
    enviar_beacon();
    lastSendTime = millis();
    interval = random(7000) + 15000;
    LoRa.receive();
  }

  if (millis() - lastSendTime2 > interval2) {
    uint32_t temp = fcsa_get_time();
    int milisegundos = temp%1000;
    int segundos = temp/1000;
    int minutos = (segundos/60);
    Serial.printf("< Timestamp: %i minutos %i segundos %i milisegundos >\r\n",minutos,segundos%60,milisegundos);

    lastSendTime2 = millis();
    interval2 = 5000;
  }
}


/******************
   PAQUETES
*/


void enviar_beacon() {
  LoRa.beginPacket();
  LoRa.write(GW_ADDR); // Source
  LoRa.write(FCSA_BEACON); // Tipo de paquete

  fcsa_beacon local;
  memset(&local, 0, sizeof(local));

  local.seq = seq;
  local.logical = fcsa_get_time();
  local.hardware = (uint32_t) millis();
  local.lrate = lrate;

  LoRa.write((uint8_t *) &local, sizeof(local));
  LoRa.endPacket();
}

void enviar_fcsa_join() {
  LoRa.beginPacket();
  LoRa.write(GW_ADDR); // Source
  LoRa.write(FCSA_JOIN); // Tipo de paquete
  LoRa.endPacket();
}

#define MAX_PACKET_LEN 512

/*
   @@@@ Contexto de interrupcion @@@@
*/
void procesar_paquete(int n) {
  if (n == 0) return;
  if (n > MAX_PACKET_LEN);

  uint8_t raw[MAX_PACKET_LEN];
  Serial.println("");
  Serial.println("@@@ Paquete recibido @@@");
  Serial.print("< ");
  for (int i = 0; i < n; i++) {
    raw[i] = LoRa.read();
    Serial.printf("0x%02hhx ", raw[i]);
  }
  Serial.print(">\r\n");
  uint8_t saddr = raw[0];
  Serial.printf("@@@ sAddr: %02hhx | len: %i @@@\r\n", saddr, n);

  switch (raw[1]) {
    case FCSA_BEACON: {
        Serial.println("### FCSA BEACON ###");
        if (n - 2 < sizeof(fcsa_beacon)) {
          Serial.println("Paquete mal formado");
          return;
        }
        fcsa_beacon *data = (fcsa_beacon*) &raw[2];

        Serial.printf("Recv Seq: %i\r\n", data->seq);
        Serial.printf("Recv Locical: %i\r\n", data->logical);
        Serial.printf("Recv Hardware: %i\r\n", data->hardware);
        Serial.printf("Recv lrate: %f\r\n", data->hardware);

        if (!(saddr < NODE_LIST_LEN)) {
          Serial.println("Nodo fuera de rango");
        }

        //Store (Hu,Hv)
        uint8_t in = nodos_tabla[saddr].in;
        nodos_tabla[saddr].local[in] = (uint32_t) millis();
        nodos_tabla[saddr].neig[in] = (uint32_t) data->hardware;
        nodos_tabla[saddr].lrate = data->lrate;
        //Serial.printf("data->lrate: %f\r\n",data->lrate);
        nodos_tabla[saddr].in++;
        nodos_tabla[saddr].in %= NODE_DATA_LEN;
        nodos_tabla[saddr].tam = nodos_tabla[saddr].tam == NODE_DATA_LEN ? NODE_DATA_LEN : nodos_tabla[saddr].tam + 1;

        uint8_t tam = nodos_tabla[saddr].tam;
        if (tam > 1) {
          Serial.printf("Calculado minimos cuadrados nodo 0x%02hhx...\r\n",saddr);
          double pendiente = 0.0;

          uint64_t Sxy = 0;
          uint64_t Sx = 0;
          uint64_t Sx2 = 0;
          uint64_t Sy = 0;
          for (int i = 0; i < tam; i++) {
            Sx += nodos_tabla[saddr].local[i];
            Sx2 += pow(nodos_tabla[saddr].local[i], 2);
            Sy += nodos_tabla[saddr].neig[i];
            Sxy += ((uint64_t) nodos_tabla[saddr].local[i]) * ((uint64_t) nodos_tabla[saddr].neig[i]);
          }

          /*Serial.printf("Sx: %"PRId64" Sx2: %"PRId64" Sy: %"PRId64" Sxy: %"PRId64"\r\n",Sx,Sx2,Sy,Sxy);

            Serial.print("local [");
            for(int i = 0;i < tam;i++){
            Serial.printf("%zu,",nodos_tabla[saddr].local[i]);
            }
            Serial.println("]");

            Serial.print("neig [");
            for(int i = 0;i < tam;i++){
            Serial.printf("%zu,",nodos_tabla[saddr].neig[i]);
            }
            Serial.println("]");*/

          pendiente = ( (double) Sxy - ((double) ((uint64_t) Sx * Sy) / ((double) tam)) ) / ( (double) (Sx2) - ((double) (pow(Sx, 2)) / ((double) tam)) );

          //Serial.printf("Pendiente: %lf\r\n",pendiente)

          Serial.printf("Hu/Hv: %lf\r\n", pendiente);
          if (pendiente > 2.0d || pendiente < -2.0d) {
            Serial.println("Esperando estabilizacion...");
            //return;
          }

          nodos_tabla[saddr].hrate = pendiente;
          nodos_tabla[saddr].activo = 1;
          

        } else {
          Serial.printf("Inicializando lista nodo 0x%02hhx...\r\n",saddr); 
        }

        double temp;
        int count = 1;
        for (int i = 0; i < NODE_LIST_LEN; i++) {
          if (nodos_tabla[i].activo == 1) {
            count++;
            Serial.printf("nodo %i hrate %f lrate %f\r\n",i,nodos_tabla[i].hrate,nodos_tabla[i].lrate);
            temp += nodos_tabla[i].hrate * nodos_tabla[i].lrate;
          }
        }
        lrate = (lrate + temp) / ((double) count);

        if (data->seq > seq) {
          Serial.printf("< @!@! Nueva ronda detectada %i (ignorando) @!@! >\r\n", data->seq);
          /*seq = data->seq;
            base = data->logical;
            lastupdate = millis();*/
        }

        Serial.printf("Local Seq: %i\r\n", seq);
        Serial.printf("Local Base: %i\r\n", base);
        Serial.printf("Local LastUpdate: %i\r\n", lastupdate);
        Serial.printf("Local Lrate: %f\r\n", lrate);
        Serial.printf("######\r\n");

      } break;
    case FCSA_JOIN: {
        Serial.println("### FCSA JOIN ###");
        if (n - 2 < 0) {
          Serial.println("Paquete mal formado");
          return;
        }
        Serial.printf("Nodo: %u\r\n", saddr);
        if (!(saddr < NODE_LIST_LEN)) {
          Serial.println("Nodo fuera de rango");
        }

        if (nodos_tabla[saddr].activo == 1 ) {
          Serial.println("Reiniciando nodo...");
          memset(&nodos_tabla[saddr], 0, sizeof(node_data));
        }
        break;
      }
    case DATA: {
        Serial.println("#### DATA PACKET ####");
        if (n - 2 < 2) { //Data len field
          Serial.println("Paquete mal formado");
          return;
        }

        data_frame *data = (data_frame*) &raw[2];

        if (data->len == 0) {
          Serial.println("# Paquete sin datos");
          break;
        }

        if (data->len != n - sizeof(data_frame) - 2) {
          Serial.println("# Cabecera mal formada (header-len != real-len)");
          break;
        }


        Serial.printf("sAddr: %u | len: %u\r\n", saddr, data->len);
        raw[2 + data->len + sizeof(data_frame)] = 0;
        Serial.printf("Content: %s\r\n", &raw[8]);


        Serial.printf("< JSON {\"saddr\":\"%u\",\"ts\":%u,\"content\":\"%s\"} >\r\n", saddr, data->ts, &raw[2 + sizeof(data_frame)]);

        break;
      }
  }
}

/*
   FCSA
*/

void init_fcsa() {
  Serial.print("FCSA INIT...");

  seq = 0;
  base = 0;
  lastupdate = 0;
  lrate = 1.0;

  fcsa_reset = 0;

  memset(nodos_tabla, 0, sizeof(nodos_tabla));

  Serial.println("OK");
}
uint32_t fcsa_get_time() {
  uint32_t res = base + (millis() - lastupdate) * lrate;
  return res;
}
