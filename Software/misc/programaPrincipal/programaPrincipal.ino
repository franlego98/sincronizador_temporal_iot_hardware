/*
 * Francisco Sanchez Lopez - 2021 Sevilla
 *
 * TFG - Universidad de Sevilla
 * 
 * Programa principal Gateway
 */

#include <SPI.h>
#include <LoRa.h>

#define csPin     15
#define resetPin  16
#define irqPin    5
#define LoraFreq  433E6  

long lastSendTime = 0;
int interval = 2000;

int nextop = -1;

void setup() {
  //Inicializar Serial
  Serial.begin(115200);
  while (!Serial);
  Serial.println("** Sincronizador temporal para IoT basado en hardware **");
  
  //Inicializar Lora
  LoRa.setPins(csPin, resetPin, irqPin);
  Serial.print("Inicializando modulo Lora ");
  if(!LoRa.begin(LoraFreq)){
    Serial.println("Fallo!");
    while (true);
  }
  Serial.println("OK!");

  Serial.print("Registrando callback ");
  LoRa.onReceive(procesar_paquete);
  Serial.println("OK");

  Serial.print("Activando escucha ");
  LoRa.receive();
  Serial.println("OK");

  Serial.println("Inicializacion completa!");
}

void loop(){
  if(millis() - lastSendTime > interval) {
    //Serial.print("Pidiendo hora...");
    lastSendTime = millis();
    interval = random(2000) + 1000;
    //LoRa.receive();
  }
}


void pedir_hora(){
  LoRa.beginPacket();
  LoRa.write(0x10); // Tipo de paquete
  LoRa.endPacket();  
}


void enviar_localtime(){
  LoRa.beginPacket();
  LoRa.write(0x11); // Tipo de paquete

  int local = millis();

  Serial.printf("Localtime: %d \r\n",local);
  LoRa.write((local>>24)&0xff);
  LoRa.write((local>>16)&0xff);
  LoRa.write((local>>8)&0xff);
  LoRa.write(local&0xff);
  
  LoRa.endPacket();  
}


void procesar_paquete(int n){
  if(n == 0) return;

  Serial.println("Paquete recibido");
  Serial.print("< ");
  for(int i = 0;i < n;i++){
    Serial.printf("%02hhx ",(uint8_t) LoRa.read()); 
  }
  Serial.print(">\r\n");
}
