### 19-4-21 ###
- Sniffer SPI FPGA funcionando a medias
- Segundo Nodo Terminado!
- Intercambiando paquete satisfactoriamente
- Recibo de paquetes mediante interrupciones

### 14-4-21 ###
- Implementando puente SPI

### 12-4-21 ###
- Test librerias SX1278 (sin FPGA) OK
- Implementando puente SPI
- Testeando modulo UART para debug

### 12-4-21 ###
- Breadboard Preparada con ESP8266 , FPGA y SX1278
- Arduino IDE funcionando
- Probando librerias (ESP8266 y SX1278 directamente)
- Probando modulos SPI (FPGA)

