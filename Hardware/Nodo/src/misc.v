module byte2ascii (
  input [7:0] i_byte,
  output reg [7:0] o_hnibble,
  output reg [7:0] o_lnibble,
  );
  
  always @(*) begin
    case (i_byte[3:0])
      4'h0: o_lnibble <= "0";
      4'h1: o_lnibble <= "1";
      4'h2: o_lnibble <= "2";
      4'h3: o_lnibble <= "3";
      4'h4: o_lnibble <= "4";
      4'h5: o_lnibble <= "5";
      4'h6: o_lnibble <= "6";
      4'h7: o_lnibble <= "7";
      4'h8: o_lnibble <= "8";
      4'h9: o_lnibble <= "9";
      4'ha: o_lnibble <= "A";
      4'hb: o_lnibble <= "B";
      4'hc: o_lnibble <= "C";
      4'hd: o_lnibble <= "D";
      4'he: o_lnibble <= "E";
      4'hf: o_lnibble <= "F";
    endcase
    
    
    case (i_byte[7:4])
      4'h0: o_hnibble <= "0";
      4'h1: o_hnibble <= "1";
      4'h2: o_hnibble <= "2";
      4'h3: o_hnibble <= "3";
      4'h4: o_hnibble <= "4";
      4'h5: o_hnibble <= "5";
      4'h6: o_hnibble <= "6";
      4'h7: o_hnibble <= "7";
      4'h8: o_hnibble <= "8";
      4'h9: o_hnibble <= "9";
      4'ha: o_hnibble <= "A";
      4'hb: o_hnibble <= "B";
      4'hc: o_hnibble <= "C";
      4'hd: o_hnibble <= "D";
      4'he: o_hnibble <= "E";
      4'hf: o_hnibble <= "F";
    endcase
  end
  
endmodule


module byte2char (
  input [7:0] i_byte,
  output reg [7:0] o_char,
  );
  
  always @(*) begin
    case (i_byte)
			8'd32: o_char <= " ";
			8'd33: o_char <= "!";
			8'd34: o_char <= "\"";
			8'd35: o_char <= "#";
			8'd36: o_char <= "$";
			8'd37: o_char <= "%";
			8'd38: o_char <= "&";
			8'd39: o_char <= "'";
			8'd40: o_char <= "(";
			8'd41: o_char <= ")";
			8'd42: o_char <= "*";
			8'd43: o_char <= "+";
			8'd44: o_char <= ",";
			8'd45: o_char <= "-";
			8'd46: o_char <= ".";
			8'd47: o_char <= "/";
			8'd48: o_char <= "0";
			8'd49: o_char <= "1";
			8'd50: o_char <= "2";
			8'd51: o_char <= "3";
			8'd52: o_char <= "4";
			8'd53: o_char <= "5";
			8'd54: o_char <= "6";
			8'd55: o_char <= "7";
			8'd56: o_char <= "8";
			8'd57: o_char <= "9";
			8'd58: o_char <= ":";
			8'd59: o_char <= ";";
			8'd60: o_char <= "<";
			8'd61: o_char <= "=";
			8'd62: o_char <= ">";
			8'd63: o_char <= "?";
			8'd64: o_char <= "@";
			8'd65: o_char <= "A";
			8'd66: o_char <= "B";
			8'd67: o_char <= "C";
			8'd68: o_char <= "D";
			8'd69: o_char <= "E";
			8'd70: o_char <= "F";
			8'd71: o_char <= "G";
			8'd72: o_char <= "H";
			8'd73: o_char <= "I";
			8'd74: o_char <= "J";
			8'd75: o_char <= "K";
			8'd76: o_char <= "L";
			8'd77: o_char <= "M";
			8'd78: o_char <= "N";
			8'd79: o_char <= "O";
			8'd80: o_char <= "P";
			8'd81: o_char <= "Q";
			8'd82: o_char <= "R";
			8'd83: o_char <= "S";
			8'd84: o_char <= "T";
			8'd85: o_char <= "U";
			8'd86: o_char <= "V";
			8'd87: o_char <= "W";
			8'd88: o_char <= "X";
			8'd89: o_char <= "Y";
			8'd90: o_char <= "Z";
			8'd91: o_char <= "[";
			8'd92: o_char <= "\\";
			8'd93: o_char <= "]";
			8'd94: o_char <= "^";
			8'd95: o_char <= "_";
			8'd96: o_char <= "`";
			8'd97: o_char <= "a";
			8'd98: o_char <= "b";
			8'd99: o_char <= "c";
			8'd100: o_char <= "d";
			8'd101: o_char <= "e";
			8'd102: o_char <= "f";
			8'd103: o_char <= "g";
			8'd104: o_char <= "h";
			8'd105: o_char <= "i";
			8'd106: o_char <= "j";
			8'd107: o_char <= "k";
			8'd108: o_char <= "l";
			8'd109: o_char <= "m";
			8'd110: o_char <= "n";
			8'd111: o_char <= "o";
			8'd112: o_char <= "p";
			8'd113: o_char <= "q";
			8'd114: o_char <= "r";
			8'd115: o_char <= "s";
			8'd116: o_char <= "t";
			8'd117: o_char <= "u";
			8'd118: o_char <= "v";
			8'd119: o_char <= "w";
			8'd120: o_char <= "x";
			8'd121: o_char <= "y";
			8'd122: o_char <= "z";
			8'd123: o_char <= "{";
			8'd124: o_char <= "|";
			8'd125: o_char <= "}";
			8'd126: o_char <= "~";
			8'd161: o_char <= "¡";
			8'd162: o_char <= "¢";
			8'd163: o_char <= "£";
			8'd164: o_char <= "¤";
			8'd165: o_char <= "¥";
			8'd166: o_char <= "¦";
			default: o_char <= ".";
		endcase
	end
endmodule
