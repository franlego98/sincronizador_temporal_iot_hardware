// look in pins.pcf for all the pin names on the TinyFPGA BX board
module top (
    input CLK,    // 16MHz clock
    output LED,   // User/boot LED next to power LED
    output USBPU,  // USB pull-up resistor
    output UART_TX,
    
    input MASTER_CS,
    input MASTER_CLK,
    input MASTER_MOSI,
    output MASTER_MISO,
    
    output LORA_CS,
    output LORA_CLK,
    output LORA_MOSI,
    input LORA_MISO,
);
    parameter TICKS_PER_BIT = 'd140;	

    // drive USB pull-up resistor to '0' to disable USB
    assign USBPU = 1;
    
    
    //BRIDGE
    wire LORA_CS = MASTER_CS;
    wire LORA_CLK = MASTER_CLK;
    wire LORA_MOSI = MASTER_MOSI;
    wire MASTER_MISO = LORA_MISO;

    reg [3:0] main_fsm_state;
    reg send_uart;
    reg [7:0] actual_char;
    reg [4:0] char_count;
    wire uart_done;
    
    reg flag1;
    reg flag2;
    
    reg [7:0] word [23:0];
  
    initial begin
      main_fsm_state = 4'h0;
      send_uart = 1'b0;
      char_count = 4'h0;
      flag1 = 0;
      flag2 = 0;
      
      word[0] = "L";
      word[1] = "A";
      word[2] = ":";
      word[3] = "0";
      word[4] = "x";
      word[5] = "_";
      word[6] = "_";
      word[7] = "/";
      word[8] = "A";
      word[9] = "L";
      word[10] = ":";
      word[11] = "0";
      word[12] = "x";
      word[13] = "_";
      word[14] = "_";
      word[15] = " ";
      word[16] = "|";
      word[17] = "_";
      word[18] = "|";
      word[19] = "_";
      word[20] = "|";
      word[21] = "\n";
      word[22] = "\r";
      word[23] = 0;
      
      
    end
    
    wire [7:0] debus1;
    wire [7:0] debus2;
    byte2ascii debug_converter_rx(
      .i_byte (buff_out[15:8]),
      .o_hnibble  (debus1),
      .o_lnibble  (debus2),
    );
    
    wire [7:0] debus3;
    wire [7:0] debus4;
    byte2ascii debug_converter_tx(
      .i_byte (buff_out[7:0]),
      .o_hnibble  (debus3),
      .o_lnibble  (debus4),
    );
    
    wire [7:0] debus5;
    byte2char debug_converter_rx_char(
      .i_byte (buff_out[15:8]),
      .o_char  (debus5)
    );
    
    wire [7:0] debus6;
    byte2char debug_converter_tx_char(
      .i_byte (buff_out[7:0]),
      .o_char  (debus6)
    );

    always @(posedge CLK) begin
      
      case(main_fsm_state)
        4'h0: begin
          char_count = 0;
          if(buff_ready == 1'b1) begin
            buff_read = 1'b1;
            main_fsm_state <= 4'h1;
          end
        end
        
        4'h1: begin
          buff_out_reg = buff_out;
          buff_read = 1'b0;
          if(char_count == 5'd5) begin
            actual_char <= debus1;
          end else if(char_count == 5'd6) begin
            actual_char <= debus2;
          end else if(char_count == 5'd13) begin
            actual_char <= debus3;
          end else if(char_count == 5'd14) begin
            actual_char <= debus4;
          end else if(char_count == 5'd17) begin
            actual_char <= debus5;
          end else if(char_count == 5'd19) begin
            actual_char <= debus6;
          end else
            actual_char <= word[char_count];
            
          send_uart = 1'b1;
          main_fsm_state <= 4'h2;
        end
        
        4'h2: begin
          if(uart_done == 1'b1) begin
            send_uart = 1'b0;
            char_count = char_count + 1;
            main_fsm_state <= 4'h3;
          end
        end
        
        4'h3: begin
          if(word[char_count] != 8'd0)
            main_fsm_state <= 4'h1;
          else
            main_fsm_state <= 4'h0;
        end
      
      endcase
    end
    
    uart_tx #(
      .TICKS_PER_BIT(TICKS_PER_BIT),
      .TICKS_PER_BIT_SIZE($bits(TICKS_PER_BIT))
    ) uart_debug (
      .i_clk    (CLK),
      .i_start  (send_uart),
      .i_data   (actual_char),
      .o_dout   (UART_TX),
      .o_done   (uart_done)
    );

    /*
    * Debugger FIFO
    */

    wire buff_ready;
    reg buff_read = 1'b0;
    wire [15:0] buff_out;

    fifo #(
      .WIDTH(16),
      .DEPTH(128)
      ) 
    debug_buff (
      .clk      (CLK),
    
      .data_in  ({spi_master_rx_data,spi_master_tx_signal == 1'b1 ? spi_master_tx_data : 8'h00}),
      .data_out (buff_out),
      
      .write    (spi_master_rx_signal),  
      .read     (buff_read),
      .fifo_not_empty (buff_ready),
      .fifo_empty (LED),
      
    );


    /*
    * SPI
    */
    
    wire spi_master_rx_signal;
    wire [7:0] spi_master_rx_data;
    
    wire spi_master_tx_signal;
    wire [7:0] spi_master_tx_data;
    
    SPI_Slave main_spi(
      .i_Rst_L     (1'b1),
      .i_Clk       (CLK),
      .o_RX_DV     (spi_master_rx_signal),
      .o_RX_Byte   (spi_master_rx_data),
    
      .i_SPI_Clk   (MASTER_CLK),
      .i_SPI_MOSI  (MASTER_MOSI),
      //.o_SPI_MISO  (MASTER_MISO),
      .i_SPI_CS_n  (MASTER_CS)
    );
    
    SPI_Slave main_spi2(
      .i_Rst_L     (1'b1),
      .i_Clk       (CLK),
      .o_RX_DV     (spi_master_tx_signal),
      .o_RX_Byte   (spi_master_tx_data),
    
      .i_SPI_Clk   (MASTER_CLK),
      .i_SPI_MOSI  (LORA_MISO),
      //.o_SPI_MISO  (MASTER_MISO),
      .i_SPI_CS_n  (MASTER_CS)
    );

endmodule

