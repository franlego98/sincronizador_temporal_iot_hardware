## Topologia de la red

Los nodos y el gateways estaran conectados de la siguiente manera:

```
	+---------+			+----+
	| Gateway | ====[ Logging ]===> | PC |
	+---------+			+----+
	    /\
	    ||
	    ||
	    ||
	    ||
	    ||
	 [ LoRa ]
	    ||
	    ||
	    ||
	    ||
	    ||
	    \/
	+-------+
	| Nodos |
	+-------+
```

### Logging

Con el principal objetivo de informar sobre los eventos que registren los nodos, el gateway se encargará de transmitir al ordenador dichos eventos asi como demas información referida al trafico en la red.

Para ello, se mostraran via UART como primera opcion.

_en versiones siguientes lo suyo seria notificarlo via wifi, por ejemplo usando una interfaz web o insertando los datos en una base de datos, como InfluxDB para luego visualizarlos con Grafana.

### LoRa

Los paquete intercambiados en la red se puede clasificar en los siguientes tipos:

- Notificacion de eventos
- Sincronizacion

Para clasificar los paquetes, se usara la siguiente estructura:

```
struct paquete {
	uint8_t opcode;
	uint8_t[] playload;
}
```

Los opcodes serán:

- 0x01 : Notificacion de eventos
- 0x02 : Sincronizacion


_Nuevos opcodes se iran añadiendo_

#### Opcodes

- ##### 0x01 : Notificacion de eventos

La información referente al evento se puede codificar en forma de JSON con los siguientes campos:

- src: nombre del nodo que recoge el evento
- ts: timestamp del evento
- desc: descripcion del evento

Execpto el gateway, los nodos deberian ignorar estos paquetes

_Con el fin de optimizar mas la red, estos paquetes podrian enviarse en formato binario siguiendo alguna estructura para evitar los caracteres que implica JSON_

- ##### 0x02 : Sincronizacion

Estos paquetes se usan a la hora de sincronizar el reloj de cada uno de los nodos, su interaccion con ellos asi como su contenido esta definido por la especificaciones del protocolo FCSA.

**TODO**




