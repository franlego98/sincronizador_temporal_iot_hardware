## EJEMPLOS DE TRAFICO

### Mensaje recibido mediante interruptiones

En bruto:
```
LA:0x12/AL:0x68 |.|h|

LA:0x00/AL:0x50 |.|P|

LA:0x92/AL:0x68 |.|h|

LA:0x50/AL:0x50 |P|P|

LA:0x13/AL:0x68 |.|h|

LA:0x00/AL:0x07 |.|.|

LA:0x10/AL:0x68 |.|h|

LA:0x00/AL:0x77 |.|w|

LA:0x8D/AL:0x68 |.|h|

LA:0x77/AL:0x77 |w|w|

LA:0x13/AL:0x68 |.|h|

LA:0x00/AL:0x07 |.|.|

LA:0x00/AL:0x68 |.|h|

LA:0x00/AL:0x68 |.|h|

LA:0x13/AL:0x65 |.|e|

LA:0x00/AL:0x07 |.|.|

LA:0x00/AL:0x65 |.|e|

LA:0x00/AL:0x65 |.|e|

LA:0x13/AL:0x6C |.|l|

LA:0x00/AL:0x07 |.|.|

LA:0x00/AL:0x6C |.|l|

LA:0x00/AL:0x6C |.|l|

LA:0x13/AL:0x6C |.|l|

LA:0x00/AL:0x07 |.|.|

LA:0x00/AL:0x6C |.|l|

LA:0x00/AL:0x6C |.|l|

LA:0x13/AL:0x6F |.|o|

LA:0x00/AL:0x07 |.|.|

LA:0x00/AL:0x6F |.|o|

LA:0x00/AL:0x6F |.|o|

LA:0x13/AL:0x20 |.| |

LA:0x00/AL:0x07 |.|.|

LA:0x00/AL:0x20 |.| |

LA:0x00/AL:0x20 |.| |

LA:0x13/AL:0x30 |.|0|

LA:0x00/AL:0x07 |.|.|

LA:0x00/AL:0x30 |.|0|

LA:0x00/AL:0x30 |.|0|

LA:0x1A/AL:0x68 |.|h|

LA:0x00/AL:0x6B |.|k|
```

Comentado:
```
LA:0x12/AL:0x68 |.|h|

LA:0x00/AL:0x50 |.|P|

Leer registro 0x12 (0x00 & 0x12) (RegIrqFlags)
RxDone y ValidHeader set
--------------------------------

LA:0x92/AL:0x68 |.|h|

LA:0x50/AL:0x50 |P|P|

Escribir registo 0x12 (0x80 & 0x12) (set to clear)
--------------------------------------------------

LA:0x13/AL:0x68 |.|h|

LA:0x00/AL:0x07 |.|.|

Leer registo 0x13 FifoRxBytesNb (Fifo rx bytes number)
7 bytes recibidos en el ultimo paquete
--------------------------------------

LA:0x10/AL:0x68 |.|h|

LA:0x00/AL:0x77 |.|w|

Leer registo 0x10 FifoRxCurrentAddr (start address of last packer received)
---------------------------------------------------------------------------

LA:0x8D/AL:0x68 |.|h|

LA:0x77/AL:0x77 |w|w|

Escribir registo 0x0d FifoAddrPtr
---------------------------------

LA:0x13/AL:0x68 |.|h|

LA:0x00/AL:0x07 |.|.|

Leer registo 0x13 FifoRxBytesNb (Fifo rx bytes number)
7 bytes recibidos en el ultimo paquete
--------------------------------------

LA:0x00/AL:0x68 |.|h|

LA:0x00/AL:0x68 |.|h|

Lee el registo 0x00, la fifo en si
Recibe el byte 0x68, osea una -> h <-
-------------------------------------

LA:0x13/AL:0x65 |.|e|

LA:0x00/AL:0x07 |.|.|

Leer registo 0x13 FifoRxBytesNb (Fifo rx bytes number)
7 bytes recibidos en el ultimo paquete
--------------------------------------

LA:0x00/AL:0x65 |.|e|

LA:0x00/AL:0x65 |.|e|

Lee el registo 0x00, la fifo en si
Recibe el byte 0x65, osea una -> e <-
-------------------------------------

LA:0x13/AL:0x6C |.|l|

LA:0x00/AL:0x07 |.|.|

Leer registo 0x13 FifoRxBytesNb (Fifo rx bytes number)
7 bytes recibidos en el ultimo paquete
--------------------------------------


LA:0x00/AL:0x6C |.|l|

LA:0x00/AL:0x6C |.|l|

Lee el registo 0x00, la fifo en si
Recibe el byte 0x6C, osea una -> l <-
-------------------------------------

LA:0x13/AL:0x6C |.|l|

LA:0x00/AL:0x07 |.|.|

Leer registo 0x13 FifoRxBytesNb (Fifo rx bytes number)
7 bytes recibidos en el ultimo paquete
--------------------------------------


LA:0x00/AL:0x6C |.|l|

LA:0x00/AL:0x6C |.|l|

Lee el registo 0x00, la fifo en si
Recibe el byte 0x6C, osea una -> l <-
-------------------------------------

LA:0x13/AL:0x6F |.|o|

LA:0x00/AL:0x07 |.|.|

Leer registo 0x13 FifoRxBytesNb (Fifo rx bytes number)
7 bytes recibidos en el ultimo paquete
--------------------------------------

LA:0x00/AL:0x6F |.|o|

LA:0x00/AL:0x6F |.|o|

Lee el registo 0x00, la fifo en si
Recibe el byte 0x6F, osea una -> o <-
-------------------------------------

LA:0x13/AL:0x20 |.| |

LA:0x00/AL:0x07 |.|.|

Leer registo 0x13 FifoRxBytesNb (Fifo rx bytes number)
7 bytes recibidos en el ultimo paquete
--------------------------------------

LA:0x00/AL:0x20 |.| |

LA:0x00/AL:0x20 |.| |

Lee el registo 0x00, la fifo en si
Recibe el byte 0x20, osea un -> espacio <-
-------------------------------------

LA:0x13/AL:0x30 |.|0|

LA:0x00/AL:0x07 |.|.|

Leer registo 0x13 FifoRxBytesNb (Fifo rx bytes number)
7 bytes recibidos en el ultimo paquete
--------------------------------------

LA:0x00/AL:0x30 |.|0|

LA:0x00/AL:0x30 |.|0|

Lee el registo 0x00, la fifo en si
Recibe el byte 0x30, osea un -> 0 <-
-------------------------------------

LA:0x1A/AL:0x68 |.|h|

LA:0x00/AL:0x6B |.|k|
```

Resumiento, se han obtenido los siguientes bytes de la fifo: h e l l o _espacio_ 0 , tal cual como se ha enviado
